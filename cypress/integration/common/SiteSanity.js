/// <reference types="Cypress" />
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

//@Given is found in sharedSteps 
When("I navigate to {string}", (urlLanguageSuffix) =>{
    if(urlLanguageSuffix !== "fr"){
        cy.visit("/" + urlLanguageSuffix)   
    }
})
Then("I should land on the Homepage in {string} and see the change to {string} label", (language,languageLabelText) => {
    cy.log("Browsing language=" + language)
    cy.contains(languageLabelText)
})