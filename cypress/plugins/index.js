/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const cucumber = require("cypress-cucumber-preprocessor").default;
const { cypressBrowserPermissionsPlugin } = require('cypress-browser-permissions')
const path = require("path");
const fs = require('fs')

module.exports = (on, config) => {
  require('cypress-mochawesome-reporter/plugin')(on);
  config = cypressBrowserPermissionsPlugin(on, config)
  on("file:preprocessor", cucumber());
  on('task', { log(message) { console.log(message); return null } });
  on('task', {
    readFileMaybe(filename) {
      if (fs.existsSync(filename)) {
        return fs.readFileSync(filename, 'utf8')
      }

      return null
    },
  });
  on('before:browser:launch', (browser, launchOptions) => {
    if (browser.name === 'chrome' && browser.isHeadless) {
      launchOptions.args.push('--disable-gpu');
    }
    if (browser.family === 'chromium' && browser.name !== 'electron') {
      launchOptions.args.push("--disable-dev-shm-usage");
      launchOptions.args.push("--no-sandbox");
    }
    return launchOptions;
  })
  return config
};

