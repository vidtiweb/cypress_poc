/// <reference types="Cypress" />
import { Given, When, Then, And, But } from "cypress-cucumber-preprocessor/steps";
const selectorById = { "Shop" : "#menuDropdown1"}
//@Givens and Ands is found in sharedSteps.js 
And("I select the {string} main menu item", (shopMenuItem) =>{
     if(shopMenuItem === "Shop" || shopMenuItem === "Magasiner"){
        cy.get(selectorById.Shop)
        .click()
        .then((promise) =>{
            //could be. a preload issue...
            //TODO: investigate --> this should have been called here cy.location('pathname').should('eq', '/en/mobile/cell-phones')
        })
    }
    else{
        throw "Menu Item not found"
    } 

    
})
And("I select the {string} menu subItem and should have been landed here {string}", (shopMenuSelectedSubitem, planUrlSuffix) =>{
    //here we have to force because of a banner for browser update ??? TODO: investigate this issue
    cy.findAllByRole("link", {name: shopMenuSelectedSubitem})
    .first()
    .click({force:true})
    
    
    // .then(() => {
    //      //kind of late to be called here but works
    //     cy.location('pathname').should('eq', planUrlSuffix)
    // })
    //kind of late to be called here but works
    //cy.location('pathname').should('eq', planUrlSuffix)
})
//@Then is in sharedSteps.js
But("the cart should be visible and populated with the selected plan", () => {
    const bothForms = cy.get('[data-drupal-selector="dxp-commerce-checkout-cart"]')
    bothForms.should('exist')

    const planDeleteButton = cy.get('[data-drupal-selector="edit-remove"]')
    planDeleteButton.should('have.length.at.most',2)
    
})
And("then I select {string}", (command) =>{
    const linkCommand = cy.findAllByRole("link", {name : command}).first()
    
    linkCommand.scrollIntoView(linkCommand)
    .click()
})
But("the cart should be populated with selected with both plan and device" , () => {
    const planDeleteButton = cy.get('[data-drupal-selector="edit-remove"]')
    planDeleteButton.should('have.length.at.most',4)
})
