/// <reference types="Cypress" />
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given("I am a user browsing in {string} navigating to {string}", (language,urlSuffix) =>{
    cy.visit(urlSuffix)
})
And("I submit with the following address {string} by executing the {string}", (address,commandText) => {
    const searchInput = cy.get("#edit-waddress-full-address")
    searchInput
        .clear()
        .type(address)
        .then(() => {
            cy.get(".pcafirstitem").first()
            .click({force:true})
            cy.findByText(commandText)
            .click()
        })
})
Then("the screen should display {string}", (expectedGreetingMessage) =>{
    cy.contains(expectedGreetingMessage)
})