@wip
Feature: Script to generate mobility bundles
    Scenario: Validate Mobility bundles without duplicates
        Given I am a command
        # TODO: Get entities from JSON
        When I launch the batch command with these entities
            | id      | price | dataInGb |
            | 976_921 | 143   | 10       |
            | 976_931 | 123   | 10       |
            | 976_941 | 108   | 10       |
            | 976_951 | 133   | 10       |
            | 976_916 | 133   | 10       |
            | 976_926 | 153   | 10       |
        Then the previous entites are processed no duplicates are detected
        
        # Then I should land on the Homepage in "<language>" and see the change to "<switchLanguageLabelText>" label

    Scenario: Validate Mobility bundles with duplicates
        Given I am a command
        When I launch the batch command with these entities
            | id      | price | dataInGb |
            | 976_921 | 143   | 10       |
            | 976_931 | 123   | 10       |
            | 976_941 | 108   | 10       |
            | 976_951 | 133   | 10       |
            | 976_916 | 133   | 10       |
            | 976_926 | 153   | 10       |
            | 976_926 | 153   | 10       |
        Then the previous entites are processed duplicates are detected
