beforeEach(() => {
    cy.blockGoogleAnalyticsCalls();
  })

Given("I am an anonymous user on a {string} device", (viewport) => {
    if(viewport !== "default"){
        cy.viewport(viewport)
    }
    cy.log("As an Anonymous user on device " + viewport)
})
When("I navigate to {string}", (urlLanguageSuffix) =>{
    if(urlLanguageSuffix !== "fr"){
        cy.visit("/" + urlLanguageSuffix)   
    }else{
        cy.visit("/")
    }
})
Then("the current url should contain {string}", (urlSuffix) =>{
    cy.location('pathname').should('contain', urlSuffix)
})
And("the product is selected by executing the {string} command", (command) => {
    const inputButton = cy.findAllByRole("button", {name : command}).first()
    
    inputButton.scrollIntoView(inputButton)
    .click()


})
When("I select the first plan with caption {string}", (planAddOperationCaption) => {
    const inputButton = cy.findAllByRole("button", {name : planAddOperationCaption}).first()
    
    inputButton.scrollIntoView(inputButton)
    .click()
})
