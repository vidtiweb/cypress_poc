#no specific Jira Issue is associated to this feature (demo test)
Feature: Web site should be running
    @smoke
    Scenario Outline: The site must be running with the following device and language configurations
        Given I am an anonymous user on a "<viewport>" device
        When I navigate to "<language>"
        Then I should land on the Homepage in "<language>" and see the change to "<switchLanguageLabelText>" label

        Examples:
            | viewport    | language | switchLanguageLabelText |
            | default     | en       | Français                |
            | iphone-x    | en       | Français                |
            | ipad-mini   | en       | Français                |
            | samsung-s10 | en       | Français                |
            | default     | fr       | English                 |
            | iphone-x    | fr       | English                 |
            | ipad-mini   | fr       | English                 |
            | samsung-s10 | fr       | English                 |

           