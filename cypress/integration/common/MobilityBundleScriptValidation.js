/// <reference types="Cypress" />
import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";

// Feature: Script to generate mobility bundles

let mobilityBundles = null

//@Given is found in sharedSteps 
Given("I am a command", () =>{
    // Nothing here
})
When("I launch the batch command with these entities", (datatable) => {
    mobilityBundles = datatable.hashes()
})
Then("the previous entites are processed no duplicates are detected", () => {
    const Ids = []
    mobilityBundles.forEach(element => {
        Ids.push(element.id)
    });
    const unique = Array.from(new Set(Ids));
    if (Ids.length === unique.length) {
        cy.log("No mobility bundle duplicates detected")
    }
})
Then("the previous entites are processed duplicates are detected", () => {
    const Ids = []
    mobilityBundles.forEach(element => {
        Ids.push(element.id)
    });
    const unique = Array.from(new Set(Ids));
    if (Ids.length !== unique.length) {
        cy.log("Mobility bundle duplicates detected")
    }
})
