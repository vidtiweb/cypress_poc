#no specific Jira Issue is associated to this feature (demo test)
@smoke
Feature: Plan and device selection path and cart validation
    Scenario Outline: The site must let me add a plan and also a device until it show checky
        Given I am an anonymous user on a "<viewport>" device
        And I navigate to "<language>"
        And I select the "<shopMenuItem>" main menu item
        And I select the "<shopMenuSelectedSubitem>" menu subItem and should have been landed here "<planUrlSuffix>"
        When I select the first plan with caption "<planAddOperationCaption>"
        Then the current url should contain "<planSecondrlSuffix>"
        But the cart should be visible and populated with the selected plan
        And then I select "<addDeviceCommand>"
        Then the current url should contain "<deviceUrlSuffix>"
        And the product is selected by executing the "<selectDevice>" command
        But the cart should be populated with selected with both plan and device
        And the product is selected by executing the "<confirmSelectionCommand>" command
        Then the current url should contain "<checkoutUrlSuffix>"
        Examples:                                                                                                                                                                                           #TODO there could be a bug here why samsung sometimes --> deviceUrlSuffix variable, this carousel is subject to change soon                                                                
            | viewport    | language | switchLanguageLabelText | shopMenuItem | shopMenuSelectedSubitem | planUrlSuffix                 | planSecondrlSuffix           | planAddOperationCaption           | deviceUrlSuffix              | addDeviceCommand | selectDevice         | confirmSelectionCommand | checkoutUrlSuffix  |
            | default     | en       | Français                | Shop         | Mobile plans            | /en/mobile/cell-phone-plans   | /en/mobile/cell-phones       | Choose this plan                  | /en/mobile/cell-phones       | Learn more       | Select this phone    | Confirm my selection    | en/mobile/checkout |
            | iphone-x    | en       | Français                | Shop         | Mobile plans            | /en/mobile/cell-phone-plans   | /en/mobile/cell-phones       | Choose this plan                  | /en/mobile/cell-phones       | Learn more       | Select this phone    | Confirm my selection    | en/mobile/checkout |
            | ipad-mini   | en       | Français                | Shop         | Mobile plans            | /en/mobile/cell-phone-plans   | /en/mobile/cell-phones       | Choose this plan                  | /en/mobile/cell-phones       | Learn more       | Select this phone    | Confirm my selection    | en/mobile/checkout |
            | samsung-s10 | en       | Français                | Shop         | Mobile plans            | /en/mobile/cell-phone-plans   | /en/mobile/cell-phones       | Choose this plan                  | /en/mobile/cell-phones       | Learn more       | Select this phone    | Confirm my selection    | en/mobile/checkout |
            | default     | fr       | English                 | Magasiner    | Forfaits Mobile         | /mobilite/forfaits-cellulaire | /mobilite/telephones-mobiles | Ajouter un téléphone à ce forfait | /mobilite/telephones-mobiles | En savoir plus   | Choisir ce téléphone | Confirmer ma sélection  | mobilite/verifier  |
            | iphone-x    | fr       | English                 | Magasiner    | Forfaits Mobile         | /mobilite/forfaits-cellulaire | /mobilite/telephones-mobiles | Ajouter un téléphone à ce forfait | /mobilite/telephones-mobiles | En savoir plus   | Choisir ce téléphone | Confirmer ma sélection  | mobilite/verifier  |
            | ipad-mini   | fr       | English                 | Magasiner    | Forfaits Mobile         | /mobilite/forfaits-cellulaire | /mobilite/telephones-mobiles | Ajouter un téléphone à ce forfait | /mobilite/telephones-mobiles | En savoir plus   | Choisir ce téléphone | Confirmer ma sélection  | mobilite/verifier  |
            | samsung-s10 | fr       | English                 | Magasiner    | Forfaits Mobile         | /mobilite/forfaits-cellulaire | /mobilite/telephones-mobiles | Ajouter un téléphone à ce forfait | /mobilite/telephones-mobiles | En savoir plus   | Choisir ce téléphone | Confirmer ma sélection  | mobilite/verifier  |

