#no specific Jira Issue is associated to this feature (demo test)
@smoke
Feature: Checky should be able to tell the user if Videotron can execute the proper adress validation in order to validate if the user can get the service or not

    Scenario Outline: Quebec User with a non-business address should be able this get the service given that this user is in a city where the service is provided
        Given I am a user browsing in "<language>" navigating to "<checkyUrlSuffix>"
        And I navigate to "<language>"
        And I select the "<shopMenuItem>" main menu item
        And I select the "<shopMenuSelectedSubitem>" menu subItem and should have been landed here "<planUrlSuffix>"
        When I select the first plan with caption "<planAddOperationCaption>"
        And the product is selected by executing the "<confirmSelectionCommand>" command
        And I submit with the following address "<address>" by executing the "<submitCommandName>"
        Then the screen should display "<expectedGreetingMessage>"
        Examples:
            | language | address                                            | expectedGreetingMessage              | checkyUrlSuffix     | shopMenuItem | shopMenuSelectedSubitem | planAddOperationCaption      | confirmSelectionCommand | submitCommandName |
            | en       | 3333 Rue Sherbrooke E, MONTRÉAL QC H1W 1C2, CANADA | Configure your phone number          | /en/mobile/checkout | Shop         | Mobile plans            | Choose only this plan        | Confirm my selection    | Continue          |
            | fr       | 3333 Rue Sherbrooke E, MONTRÉAL QC H1W 1C2, CANADA | Configurez votre numéro de téléphone | /mobilite/verifier  | Magasiner    | Forfaits Mobile         | Choisir ce forfait seulement | Confirmer ma sélection  | Continuer         |

    Scenario Outline: Sydney Nova Scotia User with a non-business address should not be able this get the service given that this user is in a city where the service is not provided
        Given I am a user browsing in "<language>" navigating to "<checkyUrlSuffix>"
        And I navigate to "<language>"
        And I select the "<shopMenuItem>" main menu item
        And I select the "<shopMenuSelectedSubitem>" menu subItem and should have been landed here "<planUrlSuffix>"
        When I select the first plan with caption "<planAddOperationCaption>"
        And the product is selected by executing the "<confirmSelectionCommand>" command
        And I submit with the following address "<address>" by executing the "<submitCommandName>"
        Then the screen should display "<expectedGreetingMessage>"
        Examples:
            | language | address                 | expectedGreetingMessage | checkyUrlSuffix     | shopMenuItem | shopMenuSelectedSubitem | planAddOperationCaption      | confirmSelectionCommand | submitCommandName |
            | en       | 150 Lingan Rd SYDNEY NS | Services not available  | /en/mobile/checkout | Shop         | Mobile plans            | Choose only this plan        | Confirm my selection    | Continue          |
            | fr       | 150 Lingan Rd SYDNEY NS | Services non offerts    | /mobilite/verifier  | Magasiner    | Forfaits Mobile         | Choisir ce forfait seulement | Confirmer ma sélection  | Continuer         |


    