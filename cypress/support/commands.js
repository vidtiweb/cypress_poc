import 'cypress-mochawesome-reporter/register';
import '@testing-library/cypress/add-commands';



Cypress.Commands.add("blockGoogleAnalyticsCalls", () => {
    cy.log('**filter and stop all Goggle Analytics calls**')
    // Do not let collect network calls get to Google Analytics. Instead intercept them
    // returning the status code 200. Since different events use different endpoints
    // let's define two intercepts to be precise
    cy.intercept('POST', 'https://www.google-analytics.com/j/collect*', { statusCode: 200 }).as('collect')
    cy.intercept('GET', 'https://www.google-analytics.com/collect*', { statusCode: 200 }).as('gifCollect')

    cy.log('**visiting the page**')
    cy.visit('/')
    // tip: cy.visit yields the window object
    // confirm the `window.ga` function has been created
    .its('ga').should('be.a', 'function')
})